# Participate in MIT survey follow-up (day 4) #

This is day 4 of the survey that you participated in the previous days.

###Duration and Compensation###

* This Day 4 survey will take _about 15 minutes_. You will be _paid $3_ for completing this survey. In addition to this base payment, you can be paid up to _$2 bonus_ depending on your performance. An average MTurker gets $1 bonus.
* There will be one more follow-up survey tomorrow. The duration and compensation of each survey will be similar to today’s survey: you will be paid up to $5 in total (minimum $3).

###Rejection###

* If you complete this task more than once (not including yesterday’s similar HIT), all your subsequent HIT submissions will be **_rejected_** except for the first one.

###Instruction###

1. Use your main PC and avoid mobile devices for the best survey experience.
1. I sent you an MTurk message with the survey link. Please email me (<djo@mit.edu>) if you haven’t gotten the message.
1. When you are finished, the survey will give you a unique completion code. Copy the code. Keep the survey window open just in case.
1. Click the link to open the HIT:  <https://worker.mturk.com/requesters/A2ECID5ZRYAIV8/projects?ref=w_pl_prvw> (or search for **"Repost: MIT survey day 4"**)
1. Accept the HIT, and enter the code from step 2 in the box in step 3. Then submit the HIT. Your HIT will be incomplete and payment will be rejected if you do not enter a valid completion code.

Please e-mail me (djo@mit.edu) if you have any question. Thank you very much!