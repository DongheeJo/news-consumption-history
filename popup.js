const sitelist = [
    "espn.com",
    "msn.com",
    "cnn.com",
    "news.google.com",
    "nytimes.com",
    "foxnews.com",
    "drudgereport.com",
    "washingtonpost.com",
    "sports.yahoo.com",
    "finance.yahoo.com",
    "buzzfeed.com",
    "usatoday.com",
    "huffingtonpost.com",
    "cnet.com",
    "businessinsider.com",
    "bbc.com",
    "dailymail.co.uk",
    "forbes.com",
    "nbcnews.com",
    "nypost.com",
    "cbssports.com",
    "npr.org",
    "bloomberg.com",
    "breitbart.com",
    "politico.com",
    "theguardian.com",
    "latimes.com",
    "cbsnews.com",
    "nydailynews.com",
    "abcnews.go.com",
    "thehill.com",
    "gizmodo.com",
    "cnbc.com",
    "tmz.com",
    "wsj.com",
    "bleacherreport.com",
    "sfgate.com",
    "people.com",
    "reuters.com",
    "usnews.com",
    "lifehacker.com",
    "popsugar.com",
    "theverge.com",
    "thedailybeast.com",
    "tomsguide.com",
    "slate.com",
    "marketwatch.com",
    "nbcsports.com",
    "maxpreps.com",
    "time.com",
    "seekingalpha.com",
    "zerohedge.com",
    "liveleak.com",
    "rollingstone.com",
    "kotaku.com",
    "news.yahoo.com",
    "si.com",
    "newsweek.com",
    "bbc.co.uk",
    "theatlantic.com",
    "vice.com",
    "chron.com",
    "pcmag.com",
    "hollywoodreporter.com",
    "telegraph.co.uk",
    "chicagotribune.com",
    "arstechnica.com",
    "dailykos.com",
    "usmagazine.com",
    "deadspin.com",
    "rawstory.com",
    "dailycaller.com",
    "tomshardware.com",
    "ksl.com",
    "digitaltrends.com",
    "eonline.com",
    "mashable.com",
    "jalopnik.com",
    "independent.co.uk",
    "pitchfork.com",
    "msnbc.com",
    "delish.com",
    "cracked.com",
    "wired.com",
    "refinery29.com",
    "complex.com",
    "newyorker.com",
    "avclub.com",
    "engadget.com",
    "ew.com",
    "thechive.com",
    "vox.com",
    "techradar.com",
    "express.co.uk",
    "centurylink.net",
    "sbnation.com",
    "nationalgeographic.com",
    "polygon.com",
    "rotoworld.com",
    "howtogeek.com",
    "wikipedia.org"
];

var History = function() {}

History.prototype.download = function(history) {
    
	// Show buffering to the user
	document.getElementById('buffering')
        .innerText = 'Generating........';
	
	// Create json file
    var historyDown = [];
	
	// Periods in days to count the # of history entries
	var pastDay = [1,2,3,7,30,90];
	var historySum = [0,0,0,0,0,0];
	var typedSum = [0,0,0,0,0,0];
	var visitSum = [0,0,0,0,0,0];
	
	for (let i = 0; i < history.length; ++i) {
	let historyURL = history[i].url;
		// Keep history from Facebook, but delete title and url
		if (historyURL.match(/facebook.com/g))
		{
			historyDown.push({
				'id': history[i].id,
				'title': "Deleted",
				'typedCount': history[i].typedCount,
				'url': "FacebookURL",
				'visitCount': history[i].visitCount,
				'visitString': new Date(history[i].lastVisitTime).toLocaleString(),
				'visitTimeStamp': history[i].lastVisitTime,
			});
		// Keep history from Twitter, but delete title and url
		} else if (historyURL.match(/twitter.com/g))
		{
			historyDown.push({
				'id': history[i].id,
				'title': "Deleted",
				'typedCount': history[i].typedCount,
				'url': "TwitterURL",
				'visitCount': history[i].visitCount,
				'visitString': new Date(history[i].lastVisitTime).toLocaleString(),
				'visitTimeStamp': history[i].lastVisitTime,
			});
		// Keep history on news media (see list above)
		} else {
			for (let j = 0; j < sitelist.length; ++j) {
				if (historyURL.includes(sitelist[j])) {
					historyDown.push({
						'id': history[i].id,
						'title': history[i].title,
						'typedCount': history[i].typedCount,
						'url': historyURL,
						'visitCount': history[i].visitCount,
						'visitString': new Date(history[i].lastVisitTime).toLocaleString(),
						'visitTimeStamp': history[i].lastVisitTime,
					});
				}
			}
		}
		// Count # of entries for each time period
		for (let k = 0; k < pastDay.length; k++) {
			var timeStampDay = new Date();
			timeStampDay.setDate(timeStampDay.getDate() - pastDay[k]);
			if (history[i].lastVisitTime > timeStampDay) {
				++historySum[k];
				typedSum[k] += history[i].typedCount
				visitSum[k] += history[i].visitCount
			}
		}
	}
	
	// Push # of entries for each time period
	for (let k = 0; k < pastDay.length; k++) {
		historyDown.push({
			'id': 100000+pastDay[k],
			'title': ("History from Day -"+pastDay[k]),
			'typedCount': typedSum[k],
			'url': "History",
			'visitCount': visitSum[k],
			'visitString': historySum[k],
			'visitTimeStamp': 0,
		});
	}
	
	// Create blob, download the json file
	var blob = new Blob(
        [JSON.stringify(historyDown, undefined, 4)], {
            type: 'application/octet-binary'
        }
    );
	var url = URL.createObjectURL(blob);
	chrome.downloads.download({
		url: url,
		filename: "news_history.json",
		saveAs: true
	});
	
}

History.prototype.generate = function() {
    var timeStamp = new Date();
	timeStamp.setDate(timeStamp.getDate()-100);
	var startTime = timeStamp.getTime();
    chrome.history.search({
        'text': '',
        'maxResults': 45000, //maximum file size of approx. 15MB
        'startTime': startTime,
    }, this.download);
}

document.addEventListener('DOMContentLoaded', function() {
    var history = new History();
    document.getElementById('gen_button')
        .onclick = function() {
            history.generate();
        };
});
