# News Consumption History

**This extension transmits absolutely no data online. Everything works locally, and no internet connection is needed for this extension to function. Look at the source code.**

This is a Chrome extension created by MIT researchers for the academic purposes.
This extension helps _locally_ creating the history of online news consumption.

Here's exactly what the extension does when you click "generate." Nothing is transmitted throughout this process.

1. The extension locally collects information about the news articles that you read online in the last 90 days (title, URL, date and time). For this, the extension searches for the history entries that contain URL of prominent news websites (see the list of websites [here](https://bitbucket.org/DongheeJo/news-consumption-history/src/d5798b1ac51733d1f2e3c72f0957a9f62743fbf5/sitelist.txt?at=master)).
1. The extension locally collects information on the frequency and timing of the visits to Facebook and Twitter. To protect privacy, page titles and URLs are completely deleted.
1. The extension counts the number of history entries for each of the last 90 days. This is to make sure that Chrome is your main browser.
1. You can then locally download the file that the extension generated. In order to complete the HIT, you are asked to upload this file in the survey. If you want to examine the content before submitting, open a new tab in your Chrome browser and drag-and-drop the file to the tab.

All rights reserved.