# Participate in MIT survey follow-up (day 5) #

This is day 5 of the survey that you participated in the previous days.

###Duration and Compensation###

* This Day 5 survey will take **_about 15 minutes_**. You will be **_paid $3_** for completing this survey. In addition to this base payment, you can be paid up to **_$2 bonus_** depending on your performance. An average MTurker gets $0.7 bonus. This HIT expires tomorrow noon (eastern time).

###Rejection###

* If you complete this task more than once (not including yesterday's similar HIT), all your subsequent HIT submissions will be **_rejected_** except for the first one.
* your HIT will be **_rejected_** if you fail to provide the latest news consumption history file. **Use your main PC and avoid mobile devices. Use Chrome browser. This survey will required the news consumption history file, same as we asked for Day 1 survey.**

###Instruction###

1. I sent you an MTurk message with the survey link. Please email me (<djo@mit.edu>) if you haven't gotten the message.
1. When you are finished, the survey will give you a unique completion code. Copy the code. Keep the survey window open just in case.
1. Click the link to open the HIT:  <https://worker.mturk.com/requesters/A2ECID5ZRYAIV8/projects?ref=w_pl_prvw> (or search for **Qualification: 8ufj7gh2**)
1. Accept the HIT, and enter the code from step 2 in the box in step 3. Then submit the HIT. Your HIT will be incomplete and payment will be rejected if you do not enter a valid completion code.

Please e-mail me (djo@mit.edu) if you have any question. Thank you very much!